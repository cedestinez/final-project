"""
Name: Mercedes Martinez
Class: ISTA131
Section: 001D Ramirez
Due: 11/26/18
Group members: Munachi Akponye, Christian Smith, Jorge Gomez Del Campo
Summary: FINAL PROJECT; Organize and display data from a csv file of statistics from popular songs on Spotify in 2017
"""
import pandas as pd 
import numpy as np 
import csv
import math
import matplotlib.pyplot as plt 

def get_data(csvfile = "featuresdf.csv"):
	""" Purpose: turn csv data file into a DataFrame and create Series objects from the columns
		Parameters: csvfile (csv file with default featuresdf.csv)
		Parameter Purpose: pass in the data file to collect info from
		Returns: DataFrame with info from csv file and Series objects from the df columns"""

	df = pd.read_csv(csvfile, header = 0, usecols = ['artists','danceability','valence'])
	s_artists = pd.Series(data = df['artists'].values)
	s_dance = pd.Series(data = df['danceability'].values)
	s_valence = pd.Series(data = df['valence'].values)
	return df, s_artists, s_dance, s_valence

def artist_fig(s_artists):
	""" Purpose: Create a horizontal bar graph showing the top 10 artists in 2017
			and how often they had a #1 song on Spotify
		Parameter: s_artists (Series object containing the artists and # of appearences)
		Parameter Purpose: pass in the Series to collect data from
		Returns: no return, only prints the horizontal bar graph of data """

	#find the top 10 artists that have the most #1 songs
	s_count = s_artists.value_counts()
	top10 = s_count[0:10]

	#create dataset
	names = top10.index.values
	count = top10.values 
	y_pos = np.arange(len(names))

	#adjust plot size
	plt.figure(figsize=(8,5))
	plt.subplots_adjust(left=0.25, right=0.75)

	#create horizontal bar plot
	plt.barh(y_pos, count, color=['violet', 'red', 'green', 'blue', 'cyan'])
	plt.yticks(y_pos, names)
	plt.xticks(np.arange(0, max(count)+1, 1.0))
	plt.title('Top 10 Spotify Artists 2017')
	plt.xlabel('Number of #1 Songs')
	plt.ylabel('Artist Name')
	plt.show()

def dance_fig(s_dance):
	""" Purpose: Create a bar graph representing the levels of danceability in songs
			on the #1 list on Spotify in 2017
		Parameter: s_dance (Series object with dancebility levels of songs)
		Parameter Purpose: pass in the data to be represented on the graph
		Returns: no return, only displays the bar graph image """

	#create dataset
	new_vals = []
	for val in s_dance.values:
		new_vals.append(round(val, 1))
	data = np.unique(new_vals, return_counts=True)
	dance = data[0]
	count = data[1]
	y_pos = np.arange(len(dance))

	#create bar plot
	plt.bar(y_pos, count, color=(0.8, 0, 0.7, 0.7))
	plt.xticks(y_pos, dance)
	plt.title('#1 Song Level of Danceability')
	plt.xlabel('Danceability Measure')
	plt.ylabel('Number of #1 Songs')
	plt.show() 

def valence_fig(df):
	""" Purpose: Create a scatter plot showing the relationship between danceability
			and valence level for songs from top Spotify artists in 2017
		Parameter: df (DataFrame with song and artist info)
		Parameter Purpose: pass in the DataFrame to take information from
		Returns: no return, only shows graph """

	artists = ['Ed Sheeran', 'The Chainsmokers', 'Drake', 'Martin Garrix']
	val, dance = [], []
	ed_v, ed_d = [], []
	chain_v, chain_d = [], []
	drake_v, drake_d = [], []
	garrix_v, garrix_d = [], []
	# collect valence and danceability data for each top artist's songs
	for i in range(len(df.index)):
		if df['artists'].iloc[i] in artists:
			val.append(df['valence'].iloc[i])
			dance.append(df['danceability'].iloc[i])
		if df['artists'].iloc[i] == 'Ed Sheeran' :
			ed_v.append(df['valence'].iloc[i])
			ed_d.append(df['danceability'].iloc[i])
		if df['artists'].iloc[i] == 'The Chainsmokers' :
			chain_v.append(df['valence'].iloc[i])
			chain_d.append(df['danceability'].iloc[i])
		if df['artists'].iloc[i] == 'Drake' :
			drake_v.append(df['valence'].iloc[i])
			drake_d.append(df['danceability'].iloc[i])
		if df['artists'].iloc[i] == 'Martin Garrix' :
			garrix_v.append(df['valence'].iloc[i])
			garrix_d.append(df['danceability'].iloc[i])

	# create scatter plot
	ed = plt.scatter(ed_d, ed_v)
	chain = plt.scatter(chain_d, chain_v)
	drake = plt.scatter(drake_d, drake_v)
	garrix = plt.scatter(garrix_d, garrix_v)
	# regression line
	z = np.polyfit(dance, val, 1)
	p = np.poly1d(z)
	plt.plot(dance, p(dance), 'r--')
	# graph labels
	plt.legend((ed, chain, drake, garrix), ('Ed Sheeran', 'The Chainsmokers', 'Drake', 'Martin Garrix'))
	plt.title("Valence and Danceability Level of Top Artists' Songs")
	plt.xlabel('Danceability')
	plt.ylabel('Valence')
	plt.show()


def main():
	df, s_artists, s_dance, s_valence = get_data()
	artist_fig(s_artists)
	dance_fig(s_dance)
	valence_fig(df)

main()