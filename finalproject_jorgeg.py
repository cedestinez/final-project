'''
final project
'''

import pandas as pd
import numpy as np
import csv
import matplotlib.pyplot as plt
import statsmodels.api as sm
from pylab import *


    
def plt1(csvf):
    df = pd.read_csv(csvf)
    slug = df['slg'].tolist()
    evelo = df['launch_speed'].tolist()
    fit=np.polyfit(slug,evelo,1)
    fit_fn=np.poly1d(fit)
    plt.plot(slug,evelo,'yo',slug,fit_fn(slug),'--k', linestyle='none',marker='o')
    plt.title('Slugging % and Exit Velocity', fontsize=25)
    plt.xlabel('Slugging %', fontsize=20)
    plt.ylabel('Exit Velocity', fontsize=20)
    plt.show()
    
def plt2(csvf):
    df = pd.read_csv(csvf)
    ba = df['ba'].tolist()
    slg = df['slg'].tolist()
    fit=np.polyfit(ba,slg,1)
    fit_fn=np.poly1d(fit)
    plt.plot(ba,slg,'yo',ba,fit_fn(ba),'--k', linestyle='none',marker='o')
    plt.title('Batting Avg. and Slugging %', fontsize=25)
    plt.xlabel('Slugging %', fontsize=20)
    plt.ylabel('Batting Average', fontsize=20)
    plt.show()
    
def plt3(csvf):
    df = pd.read_csv(csvf)
    woba = df['woba'].tolist()
    slg = df['slg'].tolist()
    fit=np.polyfit(slg,woba,1)
    fit_fn=np.poly1d(fit)
    plt.plot(slg,woba,'yo',slg,fit_fn(slg),'--k', linestyle='none',marker='o')
    plt.title('Slugging % vs. wOBA', fontsize=25)
    plt.xlabel('Slugging %', fontsize=20)
    plt.ylabel('wOBA', fontsize=20)
    plt.show()
    
def main():
    plt1('baseball.csv')
    plt2('baseball.csv')
    plt3('baseball.csv')
if __name__ == "__main__":
    main()