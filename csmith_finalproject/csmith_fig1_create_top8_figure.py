'''
Author: Christian Smith
Section Leader: Natalie Ramirez
Date: 11/29/2018
Summary: This program takes the Pokemon teams of the top 8 players from the
  Pokemon World Championships VGC 2018 and plots the frequency of each Pokemon
  in a bar graph.

'''
import pandas as pd, numpy as np
import matplotlib.pyplot as plt
from collections import Counter

# FIGURE 1
def create_top8_figure():
    '''
    Input:
        "csmith_data/vgc2018_teams.csv", a csv file containing data on the
          Pokemon teams of the top 8 players at the PW Championships VGC 2018.
          
    Output:
        A bar graph displaying the frequency of each Pokemon's occurence.
    
    '''
    vgc_teams = pd.read_csv("csmith_data/vgc2018_teams.csv", index_col=0)
    poke_list = ["P1", "P2", "P3", "P4", "P5", "P6"]
    lst = []
    for player in vgc_teams.index:
        for poke in poke_list:
            lst.append(vgc_teams.loc[player, poke])

    counts = sorted(dict(Counter(lst)).items(), key=lambda kv: kv[1])
    counts.reverse()
    counts = counts[:8]
    
    fig, ax1 = plt.subplots(num=1, figsize=(15, 7), facecolor='lightgrey')
    
    ax2 = ax1.twinx()
    ax1.set_ylabel("Frequency of Usage ", fontsize=28, labelpad=20,          \
        fontname="Tahoma")
    ax2.set_ylabel("Estimated Probability\nDistribution", fontsize=28,       \
        labelpad=75, fontname="Tahoma", rotation=270)
    ax1.set_xlabel("Pokémon", fontsize=30, labelpad=20, fontname="Tahoma")
    plt.title("Pokémon Usage in the Top 8 Teams", fontsize=36, pad=20,       \
        fontname='Tahoma')
    
    ax1.tick_params(axis='both', length=6, width=2, labelsize=18)
    ax2.tick_params(axis='y', length=6, width=2, labelsize=18)
    
    colors = ['orange', 'red', 'black', 'yellow', 'blue', 'green', 'pink',   \
        'silver']
    
    ax1.bar([c[0] for c in counts], [c[1] for c in counts])
    ax2.bar([c[0] for c in counts], [c[1]/8 for c in counts], color=colors,  \
        edgecolor='black', linewidth=2)
    ax2.set_yticks([0, 0.125, 0.25, 0.375, 0.5, 0.625, 0.75])
    
    ax1.grid(True, axis='y', alpha=0.5)
    plt.tight_layout()
    plt.show()

create_top8_figure()