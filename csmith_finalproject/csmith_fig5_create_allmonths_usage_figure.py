'''
Author: Christian Smith
Section Leader: Natalie Ramirez
Date: 11/29/2018
Summary: This program takes data from the Pokemon Showdown website on the
  usage of different Pokemon from January 2018 to October 2018 and plots
  the usage percentages of the ten most-used Pokemon in each month.

'''
import pandas as pd, numpy as np
import matplotlib.pyplot as plt

# FIGURE 5
def create_allmonths_usage_figure():
    '''
    Input:
        "csmith_data/chaos/[MONTH]_gen7vgc2018-1760.json", a group of JSON
          files from Pokemon Showdown containing data on almost all Pokemon
          from January 2018 to October 2018.
          
    Output:
        A graph of 17 lines tracking the usage percentages of the ten
          most-used Pokemon in each month.
    
    '''
    lst = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10"]
    dataframes = []
    for s in lst:
        dataframes.append(pd.read_json('csmith_data/chaos/' + s + '_gen7v' + \
            'gc2018-1760.json')[:-5].drop(axis=1, labels='info'))
    
    # Every frame in dataframes represents the statistics from a different
    # month. We're iterating over all ten months here. `i` keeps track of
    # which month we're currently on. pokedex tracks every usage percentage
    # across all months.
    pokedex = {}
    i = 1
    for df in dataframes:
        usage_dex = {}
        for pokemon in df.index:
            usage_dex[pokemon] = round(df.loc[pokemon, 'data']['usage'], 8)
        
        # Sorting by usage percentages to give the most-used Pokemon first and
        # least-used Pokemon last.
        sorted_usage = sorted(usage_dex.items(), key=lambda kv: kv[1])[-10:]
        sorted_usage.reverse()
        
        for tup in sorted_usage:
            pokemon, usage = tup[0], tup[1]
            if pokemon not in pokedex:
                pokedex[pokemon] = [(i, usage)]
            else:
                pokedex[pokemon].append((i, usage))
        i += 1
            
    # print(pokedex)
    
    fig, ax = plt.subplots(num=5, figsize=(15, 7), facecolor='lightgrey')
    ax.set_ylabel("Probability of Usage (%)", fontsize=24, labelpad=10,      \
        fontname='Tahoma')
    ax.set_xlabel("Month", fontsize=24, labelpad=10, fontname='Tahoma')
    ax.tick_params(axis='y', length=6, width=2, labelsize=16)
    ax.set_xticks(range(1, 11))
    ax.set_xticklabels(["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul",     \
        "Aug", "Sep", "Oct"], fontsize=16)
    
    colors = ['orange', '#0e2f44', 'xkcd:dirty yellow', '#afeeee',           \
        'magenta', 'purple', '#ffa500', 'silver', '#ffb6c1', 'black', 'red', \
        '#065535', 'green', '#191970', 'brown', '#ff4444', '#ffc3a0']
    
    i = 0
    for pokemon in pokedex:
        data = pokedex[pokemon]
        month_list, usage_list = [pair[0] for pair in data], [pair[1] for    \
            pair in data]
        ms = 10
        if pokemon == "Incineroar":
            ms = 15
        plt.plot(month_list, usage_list, color=colors[i], linewidth=5,       \
            label=pokemon, marker='o', markersize=ms, mew=2, mec='black')
        i += 1
    
    plt.title("Change in Pokémon Usage on Pokémon\nShowdown Across 10 " +    \
        "Months", pad=10, fontsize=30, fontname='Tahoma')
    plt.legend(loc=1, bbox_to_anchor=(1.265, 1.02), fontsize=16)
    plt.grid(True, linestyle='--')
    plt.show()

create_allmonths_usage_figure()