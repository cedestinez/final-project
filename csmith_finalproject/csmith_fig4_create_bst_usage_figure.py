'''
Author: Christian Smith
Section Leader: Natalie Ramirez
Date: 11/29/2018
Summary: This program takes data from the Pokemon Showdown website on the
  usage of different Pokemon in September and data from Kaggle on the Base
  Stat Totals of different Pokemon and creates a scatter plot comparing
  the two.

'''
import pandas as pd, numpy as np
import matplotlib.pyplot as plt

# FIGURE 4
def get_bst_usage_figure():
    '''
    Input:
        "csmith_data/09_gen7vgc2018-1760.txt", a text file from Pokemon
          Showdown containing the usage data of almost every Pokemon used
          in September.
        "csmith_data/pokemon_data.csv", a csv file from Kaggle containing the
          Base Stat Total data of almost every Pokemon.
          
    Output:
        A scatter plot that compares the Base Stat Total of each pokemon with
          their usage on Pokemon Showdown.
    
    '''
    # Step 1: Open the filename and read the text file. Skip the first
    # few lines and the last line.
    with open("csmith_data/09_gen7vgc2018-1760.txt") as fp:
        new_text = fp.readlines()[5:-1]
    
    # Step 2: Read through the text file line-by-line and grab the name of
    # each Pokemon and their usage percentage. Convert the usage percentage
    # to a float and round it. Finally, assign each Pokemon to a Series with
    # their name as the key and their usage as the value.
    usage_data = pd.Series()
    for line in new_text:
        line = line.split('|')
        pokemon, usage = line[2].strip(), line[3].strip()[:-1]
        usage_data[pokemon] = round(float(usage), 6)
    
    # Step 3: Certain Pokemon are restricted from use, don't have data, or are
    # otherwise identical to their counterparts. To avoid polluting the data,
    # we're going to drop them from the series. Most names have hyphens to
    # designate specific forms, so we're going to split on those.
    restricted_pokemon = ["Mewtwo", "Mew", "Lugia", "Ho-oh", "Celebi",       \
        "Kyogre", "Groudon", "Rayquaza", "Jirachi", "Deoxys", "Dialga",      \
        "Palkia", "Giratina", "Phione", "Manaphy", "Darkrai", "Shaymin",     \
        "Arceus", "Victini", "Reshiram", "Zekrom", "Kyurem", "Keldeo",       \
        "Meloetta", "Genesect", "Xerneas", "Yveltal", "Zygarde", "Diancie",  \
        "Hoopa", "Volcanion", "Cosmog", "Cosmoem", "Solgaleo", "Lunala",     \
        "Necrozma", "Magearna", "Marshadow", "Ash-Greninja"]
    for name in usage_data.index:
        base = name.split('-')[0]
        if base in restricted_pokemon or \
            ("Primal" in name) or ("Alola" in name) or ("Totem" in name)     \
            or ("Silvally" in name and name != "Silvally")                   \
            or (name == "Ho-oh") or (name == "Ash-Greninja"):
            usage_data = usage_data.drop(name)
    
    # Step 4: Parse the Base Stat Total (or BST) data for all Pokemon. Add
    # each Pokemon's BST and usage percentage to the two lists such that
    # each Pokemon's BST and usage are at the same indices.
    bst_data = pd.read_csv("csmith_data/pokemon_data.csv", index_col="Name", \
        usecols=["Name", "Total"]).sort_values(by='Total', ascending=False)
    bst_list, usage_list = [], []
    
    for pokemon in usage_data.index:
        bst_list.append(bst_data.loc[pokemon][0])
        usage_list.append(usage_data.loc[pokemon])
    
    # Step 5: Finally, define the dimensions of the figure, create the
    # scatter plot, and plot its regression line.
    fig, axes = plt.subplots(num=3, figsize=(15, 7), facecolor='lightgrey')
    axes.set_ylabel("Probability\nof Usage (%)", fontsize=28, labelpad=20,   \
        fontname="Tahoma")
    axes.set_xlabel("Base Stat Total", fontsize=28, labelpad=10,             \
        fontname="Tahoma")
    axes.tick_params(axis='both', length=6, width=2, labelsize=18)
    plt.title("Pokémon Base Stat Total vs. Usage in September 2018",         \
        fontsize=30, pad=20, fontname='Tahoma')
    
    plt.scatter(bst_list, usage_list, facecolor="green", edgecolor='black')
    plt.autoscale(False)
    m, b = np.polyfit(bst_list, usage_list, 1)
    plt.plot([150, 800], [b, 800*m+b], color='red', linestyle='--')
    
    arrow_params = {'facecolor':'black', 'shrink':0.11}
    axes.text(175, 52, "r-squared: 0.047", fontsize=20, fontname='Tahoma',   \
        bbox={'facecolor':'red', 'alpha':0.4, 'pad':10})
    axes.text(175, 46.5, "equation: y = (0.011)*x + (-4.097)", fontsize=20,  \
        fontname='Tahoma', bbox={'facecolor':'red', 'alpha':0.4, 'pad':10})

    axes.annotate("regression line", xy=(675, 3.233), xytext=(-30, 70),      \
        arrowprops=arrow_params, textcoords='offset points', fontsize=20,    \
            fontname='Tahoma', horizontalalignment='center')
    axes.annotate("Incineroar", xy=(530, 57.97233), xytext=(455, 58),        \
        arrowprops=arrow_params, fontsize=16, fontname='Tahoma',             \
        verticalalignment='center')
    axes.annotate("Landorus-Therian", xy=(600, 54.15734), xytext=(490, 54),  \
        arrowprops=arrow_params, fontsize=16, fontname='Tahoma',             \
        verticalalignment='center')
    axes.annotate("Tapu Koko", xy=(570, 42.00022), xytext=(490, 43.1),       \
        arrowprops=arrow_params, fontsize=16, fontname='Tahoma',             \
        verticalalignment='center')
    
    plt.grid(True)
    plt.tight_layout()
    plt.show()

get_bst_usage_figure()
