'''
Author: Christian Smith
Section Leader: Natalie Ramirez
Date: 11/29/2018
Summary: This program takes data from the Pokemon Showdown website on the
  usage of different Pokemon in different months and plots the percentage
  of each Pokemon's occurrence in a bar graph.

'''
import pandas as pd, numpy as np
import matplotlib.pyplot as plt

# FIGURES 2 and 3
def create_1760_figure(month):
    '''
    Input:
        "csmith_data/chaos/[MONTH]_gen7vgc2018-1760.json", a group of JSON
          files containing data on the usage of different Pokemon in a
          certain month.
          
    Output:
        A bar graph displaying the percentage of each Pokemon's occurence
          compared to all Pokemon on Pokemon Showdown.
    
    '''
    # Reading the JSON file that contains the usage data and grabbing it.
    df = pd.read_json("csmith_data/chaos/" + str(month) + "_gen7vgc2018-1" + \
        "760.json")[:-5].drop(axis=1, labels='info')
    usage_dex = {}
    for pokemon in df.index:
        u = df.loc[pokemon, 'data']['usage']
        usage_dex[pokemon] = round(u, 6)
    
    # Sorting the usage dictionary by the usage of each Pokemon,
    # not their names.
    usage_dex = sorted(usage_dex.items(), key=lambda kv: kv[1])[-8:]
    usage_dex.reverse()
    
    months = {"01": "January", "02": "February", "03": "March", "04":        \
        "April", "05": "May", "06": "June", "07": "July", "08": "August",    \
        "09": "September", "10": "October"}
    
    # Setting the title and the parameters for the axes and labels.
    fig, ax = plt.subplots(num=2, figsize=(15, 7), facecolor='lightgrey')
    ax.set_xlabel("Pokémon", fontsize=30, labelpad=20, fontname="Tahoma")
    ax.set_ylabel("Usage Percentage (%)", fontsize=24, labelpad=10,          \
        fontname='Tahoma')
    plt.title("Pokémon Usage Among Top Players\non Pokémon Showdown in " +   \
        months[month], fontsize=36, pad=20, fontname='Tahoma')
    ax.tick_params(axis='x', length=6, width=2, labelsize=13)
    ax.tick_params(axis='y', length=6, width=2, labelsize=18)
    
    # Aesthetic changes and plotting the bar graph.
    colors = ['red', 'orange', 'yellow', 'green', 'blue', 'indigo', 'violet',\
        'white']    
    rects = plt.bar([poke[0] for poke in usage_dex], [poke[1] for poke in    \
        usage_dex], color=colors, edgecolor='black', linewidth=2)
    
    # Placing text containing height of each bar over each bar.
    for rect in rects:
        width, height = rect.get_width(), rect.get_height()
        plt.text(rect.get_x() + width/2, height+.013, height, fontsize=16,   \
            fontname='Tahoma', ha='center', va='center')
    
    # Displaying the plot.
    plt.grid(True, axis='y', alpha=0.5)
    plt.tight_layout()
    plt.show()

create_1760_figure("09")
create_1760_figure("01")